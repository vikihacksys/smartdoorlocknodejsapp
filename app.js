//Users Microservices

const express = require('express');
const morgan = require('morgan'); 
const bodyParser = require('body-parser');
const app = express();

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
 

// app.use((req, res, next) => {
// 	res.header("Access-Control-Allow-Origin", "*")
// 	res.header(
// 		"Access-Control-Allow-Headers",
// 		"Origin, X-Requested-With, Content-Type, Accept, Authorization"
// 	)
// 	console.log("outside " + req.method) 
// 	if (req.method === "GET" || req.method === "POST" || req.method === "PUT" || req.method === "PATC" || req.method === "DELETE") {
// 		console.log("inside")
// 		res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE')
// 		// return res.status(200).json({})
// 	}
// })

const smartDoorLockUnlockRoutes = require('./api/routes/smartDoorLockUnlock') 

app.use('/setDoorLockStatus', smartDoorLockUnlockRoutes) 

app.use((req, res, next) => {
	const error = new Error('Not found')
	error.status = 404
	next(error)
})

app.use((error, req, res, next) => {
	res.status(error.status || 500)
	res.json({
		error: {
			message: error.message
		}
	})
})
 
module.exports = app;