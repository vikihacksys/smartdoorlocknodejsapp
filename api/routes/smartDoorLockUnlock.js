'use strict';

const express = require('express');
const router = express.Router(); 
const request = require("request");

var Mqtt = require('azure-iot-device-mqtt').Mqtt;
var DeviceClient = require('azure-iot-device').Client
var Message = require('azure-iot-device').Message; 

var connectionString = 'HostName=mysmartlockiothub.azure-devices.net;DeviceId=MyNodeDevice;SharedAccessKey=6L4hROTQxB7XEdN2BMXUDXWIoyUswgKm8ylck+nx/v8=';

var client = DeviceClient.fromConnectionString(connectionString, Mqtt);

/* 

	API: "http://localhost:8080/setDoorLockStatus/"

	Parameters: 
		door_lock_status: 1/0

	HTTP-method: POST	

*/

router.post('/', (req, res, next) => {

	console.log("here...")

	var message = new Message(JSON.stringify({

		doorLockStatus: req.body.door_lock_status 

	}));

    // Add a custom application property to the message.
	// An IoT hub can filter on these properties without access to the message body.
	// message.properties.add('temperatureAlert', (temperature > 30) ? 'true' : 'false');

	console.log('Sending message: ' + message.getData());

	// Send the message.
	client.sendEvent(message, function (err) {

		if (err) {

			console.error('send error: ' + err.toString());
			res.status(200).json({ 
				status: false,
				message: err.toString(),
				data: {} 
			})

		} else {
			console.log('message sent');
			res.status(200).json({ 
				status: true,
				message: "Message sent successfully.",
				data: {} 
			})
		}

	});
  
})
 
module.exports = router